#ifndef _FUNCTIONS_HPP      // protect against duplicate code error
#define _FUNCTIONS_HPP      // protect against duplicate code error

#include <string>
#include <vector>
#include <list>
using namespace std;

// STRUCT DECLARATION (Do not modify)

struct Product
{
    Product() {}
    Product( string n, float p ) : name(n), price(p) {}
    string name;
    float price;
};

struct Customer
{
    Customer() { }
    string name;
    Product order;
    int id;
};

enum Action             { LINEUP, SERVE, RAGEQUIT, COUPON };
const vector<Product> ORDER = {
    Product( "STRAWBERRY", 5.55 ),
    Product( "BLUEBERRY", 4.95 ),
    Product( "CHOCOLATE", 6.00 ),
    Product( "CUSTARD", 6.50 ),
    Product( "CARAMEL", 2.95 ) };

const vector<string> NAMES = {
    "Mohamed", "Dan", "Ahmed", "Andrea", "Ali", "Cecelia",
    "Pierre", "Cody", "Audrey", "Tyler", "Donghun", "Kayla",
    "Chen", "Samuel", "Mehmet", "Terik", "Benjamin", "Timothy",
    "Lake", "Mariam", "Madison", "Sen", "Garrin", "Franklin",
    "John", "Lauren", "Chase", "Jimmy", "Matt", "Blake",
    "Jeremiah", "Zach", "Josh", "Rachel", "Thomas", "Jolie",
    "Ruslan", "Paula", "Ethan", "Deokjin", "Uttam"
};

// FUNCTION DECLARATIONS (Do not modify)

void CustomerEntersBackOfLine( list<Customer>& line, Customer customer );
void CustomerEntersFrontOfLine( list<Customer>& line, Customer customer );
void CustomerOrders( list<Customer>& line );
void CustomerRagequits( list<Customer>& line );
Customer GetLineFront( list<Customer>& line );
Customer GetLineBack( list<Customer>& line );
string GetLine( list<Customer>& line );


/***********************************************/
/** Program and Test runners ******************/

void Program();
void RunTests();

/***********************************************/
/** Utilities *********************************/

string B2S( bool val );
void ClearScreen();
void Pause();
Customer MakeRandomCustomer( int id );

/***********************************************/
/** Test functions ****************************/

void Test_Set1();
void Test_Set2();
void Test_Set3();
void Test_Set4();
void Test_Set5();
void Test_Set6();
void Test_Set7();

const int headerWidth = 70;
const int pfWidth = 10;

#endif                      // protect against duplicate code error
